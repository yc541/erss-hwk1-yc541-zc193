from django import forms
from django.contrib.auth.models import User
from .models import Vehicle, Ride
from django.contrib.admin import widgets  



class VehicleForm(forms.ModelForm):
    owner=User
    driver_name = forms.CharField(required=True)
    car_type = forms.CharField(required=True)
    license_number = forms.CharField(required=True)
    capacity = forms.IntegerField(required=True)
    description = forms.CharField()
    
    class Meta:
        model = Vehicle 
        fields = ['owner','driver_name','car_type', 'license_number', 'capacity', 'description']
    
    def save(self, commit=True):
        v = super(VehicleForm, self).save(commit=False)
        if commit:
            v.save()
        return v

class UpdateUserInfoForm(forms.ModelForm):
    username = forms.CharField()
    email = forms.EmailField()
    
    class Meta:
        model = User
        fields = ['username','email']
    
class DateTimeInput(forms.DateTimeInput):
    input_type = 'datetime-local'
    
    
class RequestForm(forms.ModelForm):
    owner = User
    dest = forms.CharField()
    time_arrive = forms.DateTimeField(widget = DateTimeInput(attrs={'type': 'datetime-local'}))
    numberOfPassenger = forms.IntegerField()
    canShare = forms.BooleanField(required=False, initial=True)
    class Meta:
        model = Ride
        fields = ['dest', 'time_arrive', 'numberOfPassenger', 'canShare']
        

    
class ShareSearchForm(forms.Form):
    dest = forms.CharField()
    from_time = forms.DateTimeField(widget = DateTimeInput(attrs={'type': 'datetime-local'}))
    to_time = forms.DateTimeField(widget = DateTimeInput(attrs={'type': 'datetime-local'}))
    numberOfPassenger = forms.IntegerField()

class DriverSearchForm(forms.Form):
    vehicle_capacity = forms.IntegerField()
    
